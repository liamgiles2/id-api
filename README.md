## FUNCTIONS

```func CalculatePacks(quantity int) map[int]int```

 CalculatePacks calculates how many packs we need to fulfill the request.

`func GetPacks(w http.ResponseWriter, r *http.Request)`
 
GetPacks handles the retrieval of packs to the user.

``func HandleCalcReq(w http.ResponseWriter, r *http.Request)``

HandleCalcReq is the main handler for the endpoint that calculates packs. Request must be in form of CalcReq

`func StreamLinePacks(packs map[int]int, packFile PackFile) map[int]int`
StreamLinePacks ensures that we use the most optimal set up of packs.

`func UpdatePacks(w http.ResponseWriter, r *http.Request)`
UpdatePacks will update the JSON file that stores pack sizes. Will default
to initial sizes every deploy atm future work could move this to either a
dynamo table or even s3 bucket.

`func WriteJSONFile(packFileToSave PackFile) error`
WriteJSONFile updates the packs.json

`func ReadPackFile() PackFile`
ReadPackFile reads the json from storage


## TYPES

`type CalcReq struct { OrderQuantity int}`
CalcReq is the expected request format to perform calculations on

`type PackFile struct { Packs []int json:"packs"}`
PackFile is the struct that corresponds to the JSON file.

`type PackItem struct { Size int json:"size"Quantity int json:"quantity" }`
PackItem is each item found within packs.

`type PackResponse struct { packs []PackItem json:"packs" }`
PackResponse is the response sent after calculating the pack quantities.

## ENDPOINTS

`GET: /packs`
Will return a list of the available pack sizes.

`POST: /packs`
Will update the list of pack sizes, accepts a body of `{"packs" : [ int ]}}` the int array must contain all values of packsizes you want, it will overwrite all of the existing. Returns an array of the new pack values

`POST: /calculate`
Will calculate the optimum number of packs needed. accepts a body of `{"OrderQuantity": int}`, will return `{ "packs": [{size: int, quantity: int }] }`


Live API can be accessed by using calling https://limitless-citadel-39645.herokuapp.com/ through a tool like postman.

There is a front end paired: https://sheltered-retreat-82388.herokuapp.com/ if you don't have a tool like postman.


NOTE: The live services may take a little while to spin up if left inactive!