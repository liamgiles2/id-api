package main

import (
	"reflect"
	"testing"
)

func TestReadFile(t *testing.T) {
	unMarshalledjson := ReadPackFile()
	if len(unMarshalledjson.Packs) == 0 {
		t.Errorf("No JSON loaded")
	}
}

func TestWriteJSON(t *testing.T) {
	newPack := PackFile{}
	newPack.Packs = append(newPack.Packs, 5000, 2000, 1000)
	WriteJSONFile(newPack)
	newFile := ReadPackFile()
	if len(newFile.Packs) != 3 {
		t.Errorf("Updating Failed")
	}

	//Revert
	oldPack := PackFile{}
	oldPack.Packs = append(oldPack.Packs, 250, 500, 1000, 2000, 5000)
	WriteJSONFile(oldPack)
	oldFile := ReadPackFile()
	if len(oldFile.Packs) != 5 {
		t.Errorf("Updating Failed")
	}

}

func TestCalculation(t *testing.T) {
	expectedA := map[int]int{
		250: 1,
	}
	mapA := CalculatePacks(250)
	eq := reflect.DeepEqual(mapA, expectedA)
	if !eq {
		t.Errorf("Calculation incorrect for mapA")
	}

	expectedB := map[int]int{
		500: 1,
	}
	mapB := CalculatePacks(251)
	eq = reflect.DeepEqual(mapB, expectedB)
	if !eq {
		t.Errorf("Calculation incorrect for mapB")
	}

	expectedC := map[int]int{
		500: 1,
		250: 1,
	}
	mapC := CalculatePacks(501)
	eq = reflect.DeepEqual(mapC, expectedC)
	if !eq {
		t.Errorf("Calculation incorrect for mapC")
	}

	expectedD := map[int]int{
		5000: 2,
		2000: 1,
		250:  1,
	}
	mapD := CalculatePacks(12001)
	eq = reflect.DeepEqual(mapD, expectedD)
	if !eq {
		t.Errorf("Calculation incorrect for mapD")
	}

	expectedE := map[int]int{
		2000: 1,
	}
	mapE := CalculatePacks(1760)
	eq = reflect.DeepEqual(mapE, expectedE)
	if !eq {
		t.Errorf("Calculation incorrect for mapE")
	}

	expectedF := map[int]int{
		1000: 1,
	}
	mapF := CalculatePacks(760)
	eq = reflect.DeepEqual(mapF, expectedF)
	if !eq {
		t.Errorf("Calculation incorrect for mapF")
	}

	expectedG := map[int]int{
		5000: 3,
		2000: 2,
	}
	mapG := CalculatePacks(19000)
	eq = reflect.DeepEqual(mapG, expectedG)
	if !eq {
		t.Errorf("Calculation incorrect for mapG")
	}

	expectedH := map[int]int{
		5000: 2,
	}

	mapH := CalculatePacks(9999)
	eq = reflect.DeepEqual(mapH, expectedH)
	if !eq {
		t.Errorf("Calculation incorrect for mapH")
	}

	expectedI := map[int]int{
		5000: 4,
	}

	mapI := CalculatePacks(19943)
	eq = reflect.DeepEqual(mapI, expectedI)
	if !eq {
		t.Errorf("Calculation incorrect for mapI")
	}

	expectedJ := map[int]int{
		5000: 3,
		2000: 2,
		500:  1,
		250:  1,
	}

	mapJ := CalculatePacks(19750)
	eq = reflect.DeepEqual(mapJ, expectedJ)
	if !eq {
		t.Errorf("Calculation incorrect for mapJ")
	}

}
