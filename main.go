package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"sort"

	"github.com/gorilla/handlers"

	"github.com/gorilla/mux"
	_ "github.com/heroku/x/hmetrics/onload"
)

// PackFile is the struct that corresponds to the JSON file.
type PackFile struct {
	Packs []int `json:"packs"`
}

// PackResponse is the response sent after calculating the pack quantities.
type PackResponse struct {
	Packs []PackItem `json:"packs"`
}

// PackItem is each item found within packs.
type PackItem struct {
	Size     int `json:"size"`
	Quantity int `json:"quantity"`
}

// CalcReq is the expected request format to perform calculations on
type CalcReq struct {
	OrderQuantity int
}

// ReadPackFile reads the json from storage
func ReadPackFile() PackFile {
	var packs PackFile
	jsonFile, err := ioutil.ReadFile("./packs.json")
	if err != nil {
		fmt.Println(err)
	}
	json.Unmarshal(jsonFile, &packs)
	return packs
}

// CalculatePacks calculates how many packs we need to fulfill the request.
func CalculatePacks(quantity int) map[int]int {
	packs := ReadPackFile()
	// Sort data highest to lowest
	sort.Slice(packs.Packs, func(a, b int) bool {
		return packs.Packs[b] < packs.Packs[a]
	})
	packsToSend := make(map[int]int)
	for quantity > 0 {
		// Decipher how many full packs we can send
		for _, size := range packs.Packs {
			count := float64(quantity) / float64(size)
			for count >= 1 {
				if quantity > 0 {
					if count >= 1 {
						if packsToSend[size] == 0 {
							packsToSend[size] = 1
						} else {
							packsToSend[size] = packsToSend[size] + 1
						}
						quantity -= size
						count = float64(quantity) / float64(size)
					}

				}
			}
		}

		// Check if the quantity we have left is less than the lowest pack size
		// and greater than 0, add that pack and clear remaning quantity.
		if quantity < packs.Packs[len(packs.Packs)-1] && quantity > 0 {
			if packsToSend[packs.Packs[len(packs.Packs)-1]] == 0 {
				packsToSend[packs.Packs[len(packs.Packs)-1]] = 1
			} else {
				packsToSend[packs.Packs[len(packs.Packs)-1]] = packsToSend[packs.Packs[len(packs.Packs)-1]] + 1
			}
			quantity = 0
		}
	}

	// Streamline the packs so that we have the most optimal set up.
	streamedPacks := StreamLinePacks(packsToSend, packs)

	return streamedPacks
}

// StreamLinePacks ensures that we use the most optimal set up of packs.
func StreamLinePacks(packs map[int]int, packFile PackFile) map[int]int {
	for size, count := range packs {
		// Check to see if we have two of the same size pack
		if count >= 2 {
			sort.Ints(packFile.Packs)
			// Look to see if the total of the two same packs exist in another pack.
			// (e.g X*250 packs, check to see if a pack of size 500 exists)
			found := PackFinder(packFile, size*count)
			if found {
				indexOfStreamlining := sort.SearchInts(packFile.Packs, size*count)
				packs[packFile.Packs[indexOfStreamlining]] = packs[packFile.Packs[indexOfStreamlining]] + 1
				delete(packs, size)
			}
		}
	}
	// As we have looped and altered, check to see if any new duplicate packs that can be streamlined
	for s, count := range packs {
		if count == 2 {
			// Some duplicate functionality but it's to ensure we don't enter an infinite loop
			found := PackFinder(packFile, s*count)
			if found {
				return StreamLinePacks(packs, packFile)
			}
			// Check if we can combine some packs.
			// E.g (2x2000) + (1x1000)
			for innerS, innerCount := range packs {
				if s != innerS {
					sum := (s * count) + (innerS * innerCount)
					suitableLargerPackFound := PackFinder(packFile, (s*count)+(innerS*innerCount))
					if suitableLargerPackFound {
						indexOfStreamlining := sort.SearchInts(packFile.Packs, sum)
						packs[packFile.Packs[indexOfStreamlining]] = packs[packFile.Packs[indexOfStreamlining]] + 1
						delete(packs, innerS)
						delete(packs, s)
					}
				}
			}
		}
	}

	return packs
}

// HandleCalcReq is the main handler for the endpoint that calculates packs.
func HandleCalcReq(w http.ResponseWriter, r *http.Request) {
	var request CalcReq
	var response PackResponse
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		fmt.Println(err)
	}
	packMap := CalculatePacks(request.OrderQuantity)
	for size, count := range packMap {
		response.Packs = append(response.Packs, PackItem{
			Size:     size,
			Quantity: count,
		})
	}
	json.NewEncoder(w).Encode(response)
}

// GetPacks handles the retrieval of packs to the user.
func GetPacks(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(ReadPackFile())
}

// WriteJSONFile updates the packs.json
func WriteJSONFile(packFileToSave PackFile) error {
	file, _ := json.MarshalIndent(packFileToSave, "", " ")
	err := ioutil.WriteFile("packs.json", file, 0644)

	if err != nil {
		return err
	}

	return nil
}

// UpdatePacks will update the JSON file that stores pack sizes. Will default to initial sizes every deploy atm
// future work could move this to either a dynamo table or even s3 bucket.
func UpdatePacks(w http.ResponseWriter, r *http.Request) {
	var packFile PackFile
	err := json.NewDecoder(r.Body).Decode(&packFile)
	if err != nil {
		fmt.Println(err)
	}

	err = WriteJSONFile(packFile)
	if err != nil {
		fmt.Println(err)
	}

	json.NewEncoder(w).Encode(ReadPackFile())
}

func main() {
	port := os.Getenv("PORT")
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/packs", GetPacks).Methods("OPTIONS", "GET")
	router.HandleFunc("/packs", UpdatePacks).Methods("POST")
	router.HandleFunc("/calculate", HandleCalcReq).Methods("OPTIONS", "POST")

	log.Fatal(http.ListenAndServe(":"+port, handlers.CORS(handlers.AllowedHeaders([]string{"Content-Type"}), handlers.AllowedMethods([]string{"GET", "POST", "OPTIONS"}), handlers.AllowedOrigins([]string{"*"}))(router)))

}

// PackFinder is a helper function to verify a value exists in the pack list.
func PackFinder(source PackFile, value int) bool {
	for _, item := range source.Packs {
		if item == value {
			return true
		}
	}
	return false
}
